# Raspad.Network


## What's going on here

This repository contains the code and data for [raspad.network](https://raspad.network) project.  Notebooks directory contains the example of how to prepare graph files depending on your aims, and data directory contains raw data for the project, and some of the processed files ready to be used for visualization.

Data were collected with [tgcrawler](https://github.com/iggisv9t/tgcrawler)

## How to visualize the graph
We recommend to use [Gephi](https://gephi.org) for preparation and [Retina](https://ouestware.gitlab.io/retina/beta/) for exploration.

GEXF files can be loaded as is into the both Gephi and Retina. CSV files can be loaded into the Gephi. In the Gephi you can configure the layout and appearance of the graph, then export it in the GEXF format that could be loaded in Retina.
